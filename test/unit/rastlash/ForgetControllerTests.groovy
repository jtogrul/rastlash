package rastlash



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ForgetController)
class ForgetControllerTests {

    void testSomething() {
       fail "Implement me"
    }
}
