<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="loggedin"/>
    <title>Səhifə əlavə et</title>

    <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>

    <script type="text/javascript">
      
      $(document).ready(function(){
        
        checklang();
        checkqeyd();
        
        $("#langselect").change(function() {
          
          checklang();
          
        });
        
        $("#qeyd-checkbox").change(function(){
          checkqeyd();
        });
        
        
      });
      
      function checklang() {
        if( $("#langselect option:selected").val() == "az") {
            $("#qeyd-vacib").hide();
            $("#qeyd-check").show();
          } else {
            $("#qeyd-checkbox").attr('checked', true);
            $("#qeyd-check").hide();
            $("#qeyd-vacib").show();
            $("#qeyd").show();
            enableeditor();
          }
      }
      
      function checkqeyd() {
        if ($("#qeyd-checkbox").is(':checked')) {
            $("#qeyd").show();
            enableeditor();
          } else {
            $("#qeyd").hide();
          }
      }
      
      function enableeditor() {
        new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','left','center','right','ol','ul','hr','forecolor','link','unlink']} ).panelInstance('qeyd-area');
      }

    </script>

  </head>
  <body>
    <h1>Səhifə əlavə et</h1>

    <div style="padding:10px;margin-bottom: 20px;background-color: #FFFF99;font-size: 13px;">
      <b>Diqqət!</b> Xahiş edirik saytların əsas səhifəsini əlavə etməyin. Bir dəfəyə saytın sadəcə bir səhifəsini əlavə etmək daha məqsədəuyğundur.<br/>
      Məsələn, <a style="color:#B75141;">maraqlisayt.com</a> əlavə etməyin, əvəzinə <a style="color:#B75141;">maraqlisayt.com/maraqli/meqale-1</a> və <a style="color:#B75141;">maraqlisayt.com/maraqli/meqale-2</a> kimi ayrı-ayrı səhifələr əlavə edin. <a href="http://blog.rastlash.com/post/32657633200/saytlar-n-lav-edilm-si-haqq-nda">Ətraflı</a>
    </div>
    
  <g:hasErrors bean="${post}">
    <div class="signup-error">
      <g:renderErrors bean="${post}"/>
    </div>
  </g:hasErrors>

  <g:form controller="add" action="webpage" class="form">
    <div class="form-field"></div>
    <div class="form-field">
      <label for="title">Başlıq:</label>
      <input type="text" name="title" value="${post?.title}" class="pretty-input"></input>
    </div>

    <div class="form-field">
      <label for="url">Ünvan:</label>
      <input type="text" name="url" value="${post?.url}" class="pretty-input"></input></div>

    <div class="form-field">
      <label>Maraq dairəsi:</label>
      <g:select id="interest" name='interest.id' class="pretty-input"
                noSelection="${['null':'Bir maraq dairəsini seçin...']}"
                from='${allInterests}'
                optionKey="id" optionValue="name"></g:select></div>


    <div class="form-field">
    <label for="lang">Dil:</label>
    <select id="langselect" name='lang' class="pretty-input">
      <option value="az">Azərbaycanca (və ya mətnsiz)</option>
      <option value="en">İngiliscə</option>
      <option value="ru">Rusca</option>
      <option value="tr">Türkcə</option>
    </select></div>
    
    <br/>

    
    <div id="qeyd-check">
      <input type="checkbox" id="qeyd-checkbox" name="hasNote"  /> Qeyd yazmaq istəyirəm<br />
    </div>


    <div id="qeyd" style="display:none;" >
      <div id="qeyd-vacib" style="display:none;" >Azərbaycan dilində olmayan səhifələr üçün həmin saytdakı məzmun haqda azərbaycanca tərcümə və ya qısa xülasə yazmaq vacibdir <br/><br/></div>
      <textarea name="strNote" id="qeyd-area" rows="15" cols="100">${post?.strNote}</textarea> 
    </div>


    <br/>
    <input class="blue-button" type="submit" value="Əlavə et"></input>
  </g:form>

</body>
</html>
