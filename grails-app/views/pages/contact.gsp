<!doctype html>
<html>
  <head>
    <meta name="layout" content="general"/>
    <title>Əlaqə | Rastlash.com</title>
  </head>
  <body style="background-color: #5E5751;">


  <g:applyLayout name="_top" />

  <div id="content">
    
    <div id="home-content">
      <br/>
      <br/>
      <h1>Təsisçilər</h1>
      <br/>
      <h2>Toğrul Cəfərli</h2>
      <i>— həmtəsisçi, proqramçı</i>
      <br/>
      — email, gtalk: ceferlitogrul [ət işarəsi] gmail.com<br/>
      — facebook: <a href="https://fb.com/jtogrul">fb.com/jtogrul</a><br/>
      — twitter: <a href="https://twitter.com/jtogrul">&#64;jTogrul</a><br/>
      <br/>
      <br/>
      <h2>Elcan Mütəllimov</h2>
      <i>— həmtəsisçi, redaktor</i>
      <br/>
      — email, gtalk: elcan.1111i [ət işarəsi] gmail.com<br/>
      — facebook: <a href="https://fb.com/CanMeArt">fb.com/CanMeArt</a><br/>
      — twitter: <a href="https://twitter.com/Eljaaan">&#64;Eljaaan</a><br/>
      <br/>
      <br/>
      <br/>
    </div>

    <div class="clearer"></div>
    
  </div>

  <g:applyLayout name="_footer" />
</body>
</html>