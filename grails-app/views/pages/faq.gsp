<!doctype html>
<html>
  <head>
    <meta name="layout" content="general"/>
    <title>Tez-tez Soruşulan Suallar | Rastlash.com</title>
  </head>
  <body style="background-color: #5E5751;">


  <g:applyLayout name="_top" />

  <div id="content">
    
    <div id="home-content">
      <h2>Səhifələr Rastlaş-a necə əlavə olunur?</h2>Rastlaş-da rastlaşacağınız bütün səhifələr istifadəçilər tərəfindən əlavə olunur. Əlavə olunan hər bir səhifənin sizə göstərilib-göstərilməməsi bu səhifənin aid olduğu maraq sahəsinin sizin hesabınızda qeyd olunmasından asılıdır. Səhifə əlavə etmək üçün hesabınıza daxil olun və "Səhifə əlavə et" düyməsini basaraq qarşınıza çıxan formanı doldurub göndərə bilərsiniz<br/><br/>
      <h2>Hansı növ məzmun əlavə edə bilərəm?</h2>Hər hansı veb səhifə - xəbər, məqalə, bloq yazısı və ya multimedia məzmunu - şəkil, səs, video əlavə edə bilərsiniz. Tək şərt məzmunun bütün yaş kateqoriyalarına uyğun olması və insanlara pis təsir edə biləcək olmamasıdır<br/><br/>
      <h2>Multimedia məzmunlarını (şəkil, səs, video) necə paylaşa bilərəm?</h2> Hal-hazırda Rastlash.com-da bu formatlı məzmunları birbaşa paylaşmağa imkan yoxdur, siz belə məzmunları ancaq başqa saytların vasitəsilə paylaşa bilərsiniz. Biz sizə video üçün <a href="http://youtube.com">YouTube</a>, səs formatları üçün <a href="http://soundcloud.com">Soundcloud</a>, şəkil formatları üçün isə <a href="http://twitpic.com/">Twitpic</a> və ya <a href="http://photobucket.com">Photobucket</a> istifadə etməyi təklif edirik. Məzmunu bu saytlara əlavə etdikdən sonra bu saytın sizə verdiyi ünvanı rastlaş-a əlavə edə bilərsiniz<br/><br/>
      <h2>Azərbaycanca olmayan səhifə əlavə edə bilərəmmi?</h2>Rastlash.com sizə təklif edir ki, azərbaycandilli səhifələrin paylaşımına üstünlük verəsiniz. Amma biz nəzərə alırıq ki, internetdə azərbaycanca olmayan külli miqdarda maraqlı və lazımlı məlumatlar var. Buna görə də Rastlaş sizə belə səhifələri paylaşmağa icazə verir. Azərbaycandilli olmayan səhifələri əlavə edərkən bu səhifədəki məzmunun mətninin qısa tərcüməsini yazmanız vacibdir. Əks halda səhifə qəbul olunmayacaq, yanlış tərcüməli səhifələr isə məzmunundan asılı olaraq silinə və ya tərcüməsi (redaktor və ya başqası tərəfindən) yenilənə bilər<br/><br/>
      <h2>Ən yaxşı məzmunlar necə seçilir?</h2>Rastlaş istifadəçilərə bəyəndikləri və bəyənmədikləri səhifələrə müsbət və ya mənfi səs vermək imkanı verir. Bu isə sistemin bütün istifadəçilərə ən yaxşı məzmunu göstərə bilməsi üçün vacibdir. Yəni, başqalarının verdiyi səsə əsasən sizə göstərəcəyimiz səhifələri təyin edirik və əksinə, sizin verdiyiniz səslər başqalarına göstəriləcək səhifələrin təyin olunmasında rol oynayır.<br/><br/>
      <h2>"Ən məşhur səhifələr" siyahısı necə tərtib olunur?</h2>Profilinizin əsas səhifəsində olan bu siyahı daima yenilənir (intervalla deyil) və həmişə son 24 saat ərzində ən çox müsbət səs almış səhifələrin siyahısını göstərir<br/><br/>
      <h2>Facebook ilə qeydiyyatdan keçərkən və ya giriş edərkən parolumu daxil etmək mənim üçün təhlükəsizdirmi?</h2>Bəli, bu Facebook-un öz təklif etdiyi bir xidmətdir və sizin buraya öz facebook parolunuzu daxil etməyiniz tam təhlükəsizdir. Bu zaman sizin parol yalnız inteqrasiyanın facebook hissəsində emal olunur və sonra düzgün olub-olmaması haqda məlumat bizim sayta göndərilir və beləcə sizin parol bizim ya hər hansı digər 3-cü alətin yaddaşına yazılmır<br/><br/>
      <h2>Niyə bəzən hesabıma daxil olduqda facebook səhifəsi ilə qarşılaşıram?</h2>Bu sizin facebook hesabınızı rastlash.com ilə əlaqələndirdiyiniz üçün və yalnız siz facebook hesabınızdan çıxış etdikdə, Rastlaş facebook tətbiqetməsinə icazəni sildikdə və ya son girişdən uzun müddət vaxt keçdikdən sonra baş verir. Əlaqələndirilmiş facebook hesabınızla əlaqəni kəsmək üçün isə profilinizdə Sosial Hesablar bölməsinə daxil ola bilərsiniz<br/><br/>
      
    </div>

    <div class="clearer"></div>
    
  </div>

  <g:applyLayout name="_footer" />
</body>
</html>