<!doctype html>
<html>
  <head>
    <meta name="layout" content="general"/>
    <title>Haqqımızda | Rastlash.com</title>
  </head>
  <body style="background-color: #5E5751;">


  <g:applyLayout name="_top" />

  <div id="content">
    
    <div id="home-content">
      <br/>
      <br/>
      <h2>Rastlash.com haqqında</h2>
      <p>Rastlaş sizi maraqlarınıza uyğun azərbaycandilli və ya Azərbaycan dilinə tərcümə olunmuş veb-səhifə və digər məzmunlarla rastlaşdıran "kəşf maşını"-dır. Siz sadəcə bizə maraq sahələrinizi deyirsiniz və və biz sizə bəlkə də heç vaxt qarşılaşmayağınız maraqlarınıza uyğun veb səhifələri, şəkilləri, videoları və s. göstərəcəyik. Burada həmçinin hər kəs müxtəlif maraq dairələrinə uyğun səhifələr əlavə edə bilər.</p>
      <br/>
      <br/>
      <br/>
    </div>

    <div class="clearer"></div>
    
  </div>

  <g:applyLayout name="_footer" />
</body>
</html>