<!doctype html>
<html>
  <head>
    <meta name="layout" content="general"/>
    <title>Sayt sahibləri üçün | Rastlash.com</title>
  </head>
  <body style="background-color: #5E5751;">


  <g:applyLayout name="_top" />

  <div id="content">
    
    <div id="home-content">
      <h2>Sayt və ya bloq sahibləri üçün təklifimiz</h2>
      <p>Əgər siz vebsayt və ya bloq sahibisinizsə və istifadəçilərinizin sizin saytın səhifələrini asanlıqla Rastlaş-a əlavə edə bilməsini istəyirsinizsə icazə verin Rast+ xidmətimizi sizə təqdim edək. Rast+ bəzi sosial şəbəkə və digər saytlardan sizə tanış olan sosial plugin-lərin (məs., Facebook-un "Like", Twitter-in "Tweet button" xidmətləri) Rastlaş.com üçün olan analoqudur</p>
      <br/>
      <h3>Rast+ düyməsinin işləmə prinsipi belədir</h3>
      <p>Siz Rast+ üçün təqdim etdiyimiz xüsusi HTML kodu öz saytınızda istədiyiniz səhifədə və istədiyiniz yerdə yerləşdirirsiniz və bundan sonra sizin səhifəni gəzən istifadəçi səhifəni bəyənərsə və onu Rastlaş.com-a əlavə etmək istəyərsə bu düyməni basması kifayətdir. Qeyd edək ki, əgər bu səhifə artıq başqa biri tərəfindən rastlaş.com-a əlavə olunduğu halda ikinci dəfə bunu edən şəxsin bu düyməni basması sadəcə onun rastlaş.com hesabı adından sizin bazaya daha öncə əlavə edilmiş səhifənizin bəyənilməsinə səbəb olur. Yəni, bu səhifə bazamızda yoxdursa əlavə olunur, varsa bəyənilir. Biz bu əməliyyata səhifəni "rast+lamaq" ("rastpluslamaq") adı vermişik və sizdən də bunu belə adlandırmağınızı xahiş edirik. Bu düyməni əlavə etmək haqda öyrənmək üçün aşağıdakı bəndi oxuyun</p>
      <br/><br/>
      
      <h2>Rast+ düyməsinin əlavə etmək üçün lazım olan kod</h2>
      
      <ol>
        <li>Düymə (ölçüləri: 80 piksel X 20 piksel)
          <img src="${resource(dir:'images',file:'rastplus-button.png')}" style="float:left;margin-right: 10px;" /> 
          <textarea cols="100" rows="7">
<!-- Bu kodu Rast+ düyməsini görmək istədiyiniz yerə yazın -->
<iframe id="rastplus-button-iframe" frameborder=0 width="80px" height="20px" ></iframe>
<script>
  var rastplusbtn=document.getElementById("rastplus-button-iframe");
  rastplusbtn.src = "http://rastlash.com/rastplus/plugin/?type=button&url="+encodeURIComponent(document.URL);
</script></textarea>
        </li>
        <li> Qutu (ölçüləri: 50 piksel X 60 piksel)
          <img src="${resource(dir:'images',file:'rastplus-box.png')}" style="float:left;margin-right: 43px;" /> 
          <textarea cols="100" rows="7">
<!-- Bu kodu Rast+ düyməsini görmək istədiyiniz yerə yazın -->
<iframe id="rastplus-box-iframe" frameborder=0 width="50px" height="60px" ></iframe>
<script>
  var rastplusbtn=document.getElementById("rastplus-box-iframe");
  rastplusbtn.src = "http://rastlash.com/rastplus/plugin/?type=box&url="+encodeURIComponent(document.URL);
</script></textarea>
        </li>
      </ol>
      
      
      <br/>
      
      <h3>Və ya düyməni öz istəyinizə uyğun formada özünüz qurun</h3>
      <textarea cols="100"><a href="http://rastlash.com/rastplus/?url=SİZİN_SƏHİFƏNİN_ÜNVANI&title=SƏHİFƏNİN_BAŞLIĞI" target="_blank">Rast+</a></textarea>
      <br/>
      <p>Yuxarıdakı kodda <code>SİZİN_SƏHİFƏNİN_ÜNVANI</code> və <code>SƏHİFƏNİN_BAŞLIĞI</code> dəyişənlərini rast+lanılacaq səhifəyə görə dəyişməlisiniz. Diqqət edilməsi vacib olan bir məqam isə odur ki, bu dəyişənləri URL-ə uyğunlaşdırılmış kodlanmış formada yazmalısınız (URL Encoding). Saytınız PHP üzərində işləyirsə URL-ə uyğunlaşdırmanı <a href="http://php.net/manual/en/function.urlencode.php"><code>urlencode()</code></a> funksiyası ilə edə bilərsiniz.  Müxtəlif proqramlaşdırma dillərində bunun necə edilməsi lazım olması haqda ətraflı məlumat əlavə olunacaq</p>
      <h3>Nümunə</h3>
      <p>Tutaq ki, siz ünvanı "http://sizinsayt.com/numune/sehife" ünvanlı və "Nünunə səhifə" başlıqlı səhifə üçün rast+ düyməsi qurmaq istəyirsiniz. Bu zaman:</p>
      <p>"<code>SİZİN_SƏHİFƏNİN_ÜNVANI</code>" dəyişənini "<code>http%3a%2f%2fsizinsayt.com%2fnumune%2fsehife</code>" ilə, "<code>SƏHİFƏNİN_BAŞLIĞI</code>" dəyişənini isə "<code>N%c3%bcnun%c9%99+s%c9%99hif%c9%99</code>" ilə əvəz etməlisiniz. Nəticə kod belə olacaq:</p>
      <textarea cols="100"><a href="http://rastlash.com/rastplus/?url=http%3a%2f%2fsizinsayt.com%2fnumune%2fsehife&title=N%c3%bcnun%c9%99+s%c9%99hif%c9%99" target="_blank">Rast+</a></textarea>

    </div>

    <div class="clearer"></div>
    
  </div>

  <g:applyLayout name="_footer" />
</body>
</html>