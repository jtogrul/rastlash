<!doctype html>
<html>
  <head>
    <meta name="layout" content="general"/>
    <title>Maraqlı biləcəyiniz bütün səhifələrlə rastlaşın! | Rastlash.com</title>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript">
      
      $(document).ready(function(){
        $("#input-login").focusout(function(){
          if($(this).val() == '') {
            $(this).val("İstifadəçi adı və ya Email");
          }
        });
        
        $("#input-login, #input-password").focus(function(){
          $(this).val("");
        });

      });
  </script>
  </head>
  <body style="background-color: #5E5751;">
   
    <div id="content">
      
      <a href="${createLink(controller:'index')}" style="margin-bottom: 100px;float:left;"><img src="${resource(dir:'images',file:'logo-dark.png')}" /></a> <br/>

      <div class="clearer"></div>
      
      <div id="home-content" style="float:left;width:450px">
        <div style="width: 450px;"><img src="${resource(dir:'images/icons',file:'question_balloon.png')}"/><div style="width: 380px;"><h2>Rastlaş nədir?</h2>Rastlaş internetdəki sizin maraq sahələrinizə uyğun ən yaxşı veb-səhifələri, video, şəkil və s. azərbaycanca məzmunları sizə təqdim edən kəşf maşınıdır. Qeydiyyatdan keçin, nələrlə maraqlandığınızı bizə deyin və sizin üçün hazırladığımız internet səyahətinə başlayın. Rastlaşdığınız ən yaxşı səhifələri dostlarınızla paylaşın və siz də öz kəşf etdiyiniz maraqlı səhifələri bizimlə bölüşün</div></div><div class="clearer"></div>
      </div>

      <div id="home-login" style="float:right;width:400px;">
        <h2>Hesaba giriş</h2>
        <div id="error">
          <g:if test="${flash.error}">
            ${flash.error}
          </g:if>
        </div>
        <g:form name="index-login" url="[action:'login',controller:'user']">
          <input id="input-login" class="pretty-input" name ="username" type="text" value="İstifadəçi adı və ya Email" ></input><br/>
          <input id="input-password" class="pretty-input" name="password" type="password" value="Şifrə"></input><br/>
          <input class="blue-button" type="submit" value="Daxil ol"></input> 
          <a href="${createLink(controller: 'forget')}" style="color:red;font-size: 12px;">Şifrəni unutmuşam</a> 
          <br/><br/><br/>
        </g:form>

        <br/>
        <a href="${createLink(action:'authent', controller:'facebook')}" class="facebook-button" style="background-color:#3D62B3;color:#fff;border:0;">Facebook ilə Daxil Ol</a>
        <a href="${createLink(action:'signup', controller:'user')}" class="blue-button" >Qeydiyyatdan keç</a> 
      </div>

      <div class="clearer"></div>

    </div>

  <g:applyLayout name="_footer" />
</body>
</html>
