<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="loggedin"/>
    <title>Baxdıqlarım</title>
  </head>
  <body>
    <h1>Tarixçə</h1>
    
    <g:if test="${likes.size() == 0}">
      <div style="padding:10px;margin-bottom: 20px;background-color: #FFFF99;">Siz hələ heç bir səhifəyə baxmamısınız. Maraqlarınıza uyğun səhifələrlə rastlaşmaq üçün sol-yuxarı küncdəki "Rastlaş!" düyməsinə basın.</div>
    </g:if>
    
  <g:each in="${likes}">
    <g:render template="/layouts/postbox" model="[it:it]"/>
  </g:each>
  
  <g:render template="/layouts/paginator" model="[action:'history']"/>
  
</body>
</html>
