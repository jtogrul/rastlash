<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="loggedin"/>
    <title>Əsas səhifə</title>
  </head>
  <body>
    
    <div style="padding:10px;margin-bottom: 20px;background-color: #FFFF99;font-size: 13px;">
      Sizin üçün seçdiyimiz veb-səhifələri gəzmək üçün sol-yuxarı küncdəki mavi "Rastlaş" düyməsini basın
    </div>
    
    <h2>Maraq sahələrinizdəki ən məşhur səhifələr</h2>
    <g:each in="${posts}">
      <g:render template="/layouts/postbox" model="[it:it]"/>
    </g:each>
</body>
</html>
