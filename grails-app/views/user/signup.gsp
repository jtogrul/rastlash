<!doctype html>
<html>
  <head>
    <meta name="layout" content="general"/>
    <title>Qeydiyyat</title>
  </head>
  <body>
    <div id="top">

      <g:applyLayout name="_top" />
      <div id="content">

        <g:applyLayout name="_signup" />

      </div>
      <div class="clearer"></div>

    </div>

  <g:applyLayout name="_footer" />
</body>
</html>
