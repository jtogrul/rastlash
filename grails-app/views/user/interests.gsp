<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="loggedin"/>
    <title>Maraqlarım</title>
  </head>
  <body>
    <h1>Maraqlarım</h1>
    
    <g:if test="${userInterests.size() == 0}">
      <div style="padding:10px;margin-bottom: 20px;background-color: #FFFF99;">Xoş gördük! Rastlaş.com-un sizə bəyənəcəyiniz səhifələri göstərməsi üçün sizin maraq sahələrinizi bilməlidir. Aşağıdakı siyahıda sizi maraqlandıran sahələrin qarşısındakı qutunu seçməklə bizə kömək ola bilərsiniz</div>
    </g:if>

    <g:form action="saveinterests">

      <g:each var="interest" in="${allInterests}">
        <g:checkBox name="${interest.shortName}" value="${interest in userInterests}" />
        <label for="${interest.shortName}">${interest.name}</label> <br/>
      </g:each>
      <br/>
      <input class="blue-button" type="submit" value="Maraqları yadda saxla"></input>
    </g:form>

</body>
</html>
