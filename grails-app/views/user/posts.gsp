<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="loggedin"/>
    <title>Əlavə etdiklərim</title>
  </head>
  <body>
    <h1>Əlavə etdiklərim</h1>

  <g:if test="${posts.size() == 0}">
    <div style="padding:10px;margin-bottom: 20px;background-color: #FFFF99;">Siz hələ heç bir səhifə əlavə etməmisiniz. Rastlash.com-da rastlaşacağınız səhifələr bazaya istifadəçilər tərəfindən əlavə edilir. Maraqlı bildiyiniz səhifələri əlavə edərək bazanın daha da zənginləşməsinə kömək edə bilərsiniz</div>
  </g:if>

  <g:each in="${posts}">
    <g:render template="/layouts/postbox" model="[it:it]"/>
  </g:each>

  <g:render template="/layouts/paginator" model="[action:'posts']"/>

</body>
</html>
