<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="loggedin"/>
    <title>Sosial hesablar</title>
  </head>
  <body>
    <h1>Sosial hesablarım</h1>

    <p>
    <h2>Facebook</h2>
  <g:if test="${user.facebookUid == null}">
    <div class="red-box box-padding">
      Əlaqələndirilmiş Facebook profiliniz yoxdur. <a href="${createLink(action:'authent', controller:'facebook')}">Əlaqələndirmək üçün buraya klikləyin</a>
    </div>
  </g:if>
  <g:else>

    <div class="green-box box-padding">

      Facebook hesabınız əlaqələndirilib. <a href="${createLink(action:'remove', controller:'facebook')}">Əlaqələni kəsmək üçün buraya klikləyin</a>
      <br/><br/>
      <g:form action="savefacebooksharing">

        <g:checkBox name="sharefacebookonlike" value="${user.shareFacebookOnLike}" />
        <label for="${sharefacebookonlike}">Zaman tuneli ilə inteqrasiya (bəyəndiyiniz səhifələr Facebook profilinizdə görünsün)</label> <br/>
        <br/>
        <input class="blue-button" type="submit" value="Yadda saxla"></input>
      </g:form>
    </div>
  </g:else>
</p>

</body>
</html>
