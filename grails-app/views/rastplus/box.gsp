<%@ page contentType="text/html;charset=UTF-8" %>


<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'rastplus-box.css')}" type="text/css">
  </head>
  <body>

    <div id="rastplus-plugin" ${active?'class="active"':''} title="Rast+la" >
      <a id="rastplus-count" ${active?'class="active"':''}>${likecount>0?likecount:'Rast+la'}</a>
      <a href="http://rastlash.com/rastplus/?url=${pageurl}" target="_blank"><div id="rastplus-button" ><img src="${resource(dir:'images',file:'btn-icon.png')}"/></div></a>
    </div>

  </body>
</html>