<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Şifrənin bərpası</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">
  </head>
  <body>
    

    <div id="single-message" >
      <h1>Şifrənin bərpası</h1>
      <g:form url="[action:'forget',controller:'forget']">
        <label for="usernameemail">İstifadəçi adı və ya emailinizi yazın</label><br/><br/>
        <input type="text" name="usernameemail" class="pretty-input"/>
        <input type="submit" class="blue-button" value="Şifrəni bərpa et"/>
      </g:form>

      <g:if test="${flash.error}">
        <div class="signup-error">
          ${flash.error}
        </div>
      </g:if>

    </div>



  </body>
</html>
