<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Yeni şifrə seçimi</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">
  </head>
  <body>
    

    <div id="single-message" >
      <h1>Yeni şifrə seçimi</h1>
      <g:form url="[action:'newpassword',controller:'forget']">
        <input type="password" name="password" class="pretty-input"/><label for="password"> Yeni şifrə seçin</label><br/>
        <input type="password" name="password2" class="pretty-input"/><label for="password2"> Yeni şifrəni təkrar yazın</label><br/>
        <g:hiddenField name="secret" value="${params.secret}" />
        <input type="submit" class="blue-button" value="Şifrəni dəyiş"/>
      </g:form>


      <g:if test="${flash.error}">
        <br/><br/>
        <div class="signup-error">
          ${flash.error}
        </div>
      </g:if>

    </div>

  </body>
</html>
