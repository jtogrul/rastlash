<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# rastlash: http://ogp.me/ns/fb/rastlash#"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <meta property="fb:app_id" content="326782320752165" /> 
    <meta property="og:type"   content="rastlash:page" /> 
    <meta property="og:url"    content="http://rastlash.com/r/${post.id}" /> 
    <meta property="og:title"  content="${post.title}" /> 
    <meta property="og:image"  content="http://dl.dropbox.com/u/62443210/fb%20profile.png" /> 


    <title>Rastlaş - ${post.title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="http://code.jquery.com/ui/1.8.23/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.8.23/themes/vader/jquery-ui.css"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css" />

    <script type="text/javascript">
      var postid = ${post.id};
      var title = "${post.title}";
      var homeurl = "${createLink(uri:'/', absolute : 'true')}";
      var hasnote = ${post.hasNote};
      var lang = "${post.lang}";
    </script>

    <script src="${resource(dir: 'js', file: 'rastla.js')}"></script>

    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-34857169-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>

  </head>
  <body class="nooverflow">
    <table cellspacing="0" cellpadding="0" style="width:100%;height:100%;">
      <tbody>
        <tr>
          <td height="40">
            <div id="rastlash-bar">
              <a id="rastlash-button" href="${loggedin? createLink(controller: 'rastla')  : createLink(controller: 'index')}" title="Növbəti səhifəni görmək üçün bu düyməni basın">Rastlaş</a>
              <div id="bar-vote">
                <span class="bar-span" title="Bu səhifəni bəyənirsinizmi?">Səs ver:</span>
                <div id="like-vote" class="vote bar-btn vote-btn ${votelike?'active-vote':''}" vote="like" title="Bəyəndim"></div>
                <div id="dislike-vote" class="vote bar-btn vote-btn ${votedislike?'active-vote':''}" vote="dislike" title="Bəyənmədim"></div>
                <a id="report-btn" class="bar-btn" href="http://rastlash.com/rastla/report/${post.id}" target="_blank" title="Şikayət et">!</a>

              </div>

              <div id="bar-share">
                <span class="bar-span" title="Bu səhifəni dostlarınızla paylaşın">Paylaş:</span>
                <img id="share-twitter" class="bar-btn" src="${resource(dir: 'images', file: 'twitter.png')}" title="Twitter-də paylaş"/>
                <img id="share-facebook" class="bar-btn" src="${resource(dir: 'images', file: 'facebook.png')}" title="Facebook-da paylaş"/>
                <img id="share-googleplus" class="bar-btn" src="${resource(dir: 'images', file: 'googleplus.png')}" title="GooglePlus-da paylaş"/>
              </div>

              <g:if test="${post.hasNote}">
                <div id="bar-qeyd" >${post.lang=="az"?"Qeydə bax":"Tərcüməyə bax"}</div>
              </g:if>

              <g:if test="${isManager}">
                <span class="bar-span" style="margin-left:40px;"><a href="${createLink(controller: 'managePost', action: 'edit', id: id)}">[edit]</a></span>
              </g:if>

              <span class="bar-span" style="margin-left:40px;">Əlavə edən: ${post.poster.username}</span>

              <a  class="bar-btn-right" href="${createLink(controller: 'home')}">Əsas səhifə</a>

            </div>
          </td>
        </tr>
        <tr>
          <td>
            <iframe style="width:100%;height:100%" frameborder="0" scrolling="auto" src="${iframeurl}" name="content_iframe" class="page_content"></iframe>
          </td>
        </tr>
      </tbody>
    </table>
  <g:if test="${post.hasNote}">
    <div id="post-note" title="${post.lang=="az"?"Qeyd":"Tərcümə"}">${post.strNote}</div>
  </g:if>
</body>
</html>
