<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Şikayət forması</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">
  </head>
  <body>

    <div id="single-message">
      <h1>Şikayət forması</h1>
      

      <g:form controller="rastla" action="report" class="form">
        
        <g:hiddenField name="postid" value="${postid}"/>
        <br/>

        <div class="form-field">
          <label for="sebeb">Səbəb: </label>
          <select name='sebeb' class="pretty-input">
            <option value="problemli_sehife">Səhifə düzgün deyil</option>
            <option value="tehlukeli_sehife">Səhifə təhlükəlidir</option>
            <option value="yash_kateqoriyasi">Məzmun hər yaş kateqoriyasına uyğun deyil</option>
            <option value="dil_problemi">Dildə və ya tərcümədə problem</option>
            <option value="diger">Digər</option>
          </select>
        </div>
        <br/>
        
        Əlavə qeyd:<br/>
        <textarea name="qeyd" rows="4" cols="50"></textarea> 

        <br/>


        <br/>
        <input class="blue-button" type="submit" value="Şikayət et"></input>
      </g:form>

    </div>
  </body>
</html>
