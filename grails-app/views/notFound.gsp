
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Səhv: 404</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">

  </head>
  <body>
    <div id="single-message" class="red-box" style="text-align: center;">
      Bu işdə bir tərslik var
      <div style="font-size:120px;">404</div>
      <h2>Sizinlə belə rastlaşmaq istəməzdik</h2>
      <br/>
      <a href="javascript:history.go(-1)">← Geri qayıt</a> / <a href="${createLink(controller:'index')}">Ana səhifə</a>
    </div>
  </body>
</html>
