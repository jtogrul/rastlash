<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${message}</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">

  </head>
  <body>
    <div id="single-message" class="${divclass}">
      ${message}
      <br/>
      <a href="javascript:history.go(-1)">← Geri qayıt</a> &#183; <a href="${createLink(controller:'index')}">Ana səhifə</a>
    </div>
  </body>
</html>
