<a class="post-box" href="${createLink(uri:'/r/'+it.id)}">
  <div class="post-box-title">${it.title}</div>
  <div class="post-box-interest">${it.interest.name}</div>
  <div class="post-box-poster">${it.poster.username}</div>
  <div class="clearer"></div>
</a>