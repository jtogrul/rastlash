<div id="profile-sidebar">
  
  <div id="profile-box">${rastlash.User.findWhere(id:session.user).fullName}</div>
  
  <a href="${createLink(controller: 'home')}">Əsas səhifə</a>
  <a href="${createLink(controller: 'profile',action:"posts")}">Əlavə etdiklərim</a>
  <a href="${createLink(controller: 'profile', action:"likes")}">Bəyəndiklərim</a>
  <a href="${createLink(controller: 'profile', action:"history")}">Baxdıqlarım</a>
  <a href="${createLink(controller: 'profile', action:"interests")}">Maraqlarım</a>
  <a href="${createLink(controller: 'profile', action:"socialaccounts")}">Sosial hesablarım</a><br/>
  <a href="${createLink(controller: 'add', action:'webpage')}" style="background-color:#FA6900;">Səhifə əlavə et</a>
</div>