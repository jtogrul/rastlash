<div id="full-signup">
  <h1>Qeydiyyatdan Keç</h1> <br/>
  <g:if test="${!session.facebookToken}">
    <a href="${createLink(action:'authent', controller:'facebook')}" class="facebook-button" style="background-color:#3D62B3;color:#fff;border:0;">Facebook ilə Qeydiyyatdan Keç</a>
    <br/><br/>
    <h2>və ya aşağıdakı formanı doldurun</h2>
    <br/>
  </g:if>
  
  <g:hasErrors bean="${user}">
    <div class="signup-error">
      <g:renderErrors bean="${user}"/>
    </div>
  </g:hasErrors>

  <g:form id="content-signup-form" name="page-signup" url="[action:'signup',controller:'user']">
    <div><input class="pretty-input ${hasErrors(bean:user,field:'fullName','input-error')}" type="text" name="fullName" value="${user?.fullName}" ></input><span>— Tam adınızı daxil edin</span></div>
    <div><input class="pretty-input ${hasErrors(bean:user,field:'username','input-error')}" type="text" name="username" value="${user?.username}" ></input><span>— İstifadəçi adı seçin</span></div>
    <div><input class="pretty-input ${hasErrors(bean:user,field:'email','input-error')}" type="email" name="email" value="${user?.email}" ></input><span>— Email ünvanınızı daxil edin</span></div>
    <div><input class="pretty-input ${hasErrors(bean:user,field:'password','input-error')}" type="password" name="password"></input><span>— Şifrə seçin</span></div>
    <div><input class="pretty-input ${hasErrors(bean:user,field:'password2','input-error')}" type="password" name="password2"></input><span>— Şifrəni təkrar yazın</span></div>

    <input class="blue-button" type="submit" value="Qeyd ol"></input> <br/>
  </g:form>
</div>