<%@ page contentType="text/html;charset=UTF-8" %>

<div id="top">

  <script type="text/javascript">
      
      $(document).ready(function(){
        $("#input-login").focusout(function(){
          if($(this).val() == '') {
            $(this).val("İstifadəçi adı və ya Email");
          }
        });
        
        $("#input-login, #input-password").focus(function(){
          $(this).val("");
        });

      });
      

      

  </script>

  <div id="home-bar">
    <div id="info">
      <a href="${createLink(controller:'index')}"><img id="logoimg" src="${resource(dir:'images',file:'logo-light.png')}" /></a>
      <div class="clearer"></div>
    </div>

    <div id="login">
      <g:if test="${session.user}">

        <a href="${createLink(action:'logout', controller:'user')}" class="blue-button" >Çıxış</a>

      </g:if>
      <g:else>

        <div id="error">
          <g:if test="${flash.error}">
            ${flash.error}
          </g:if>
        </div>
        <g:form name="index-login" url="[action:'login',controller:'user']">
          <input id="input-login" class="pretty-input" name ="username" type="text" value="İstifadəçi adı və ya Email" ></input>
          <input id="input-password" class="pretty-input" name="password" type="password" value="Şifrə"></input>
          <input class="blue-button" type="submit" value="Daxil ol"></input> <br/>
          <a href="${createLink(controller: 'forget')}" style="color:#fff;margin-right:80px;float: right;font-size: 12px;">Şifrəni unutmuşam</a>
        </g:form>

        <br/>
        <a href="${createLink(action:'authent', controller:'facebook')}" class="facebook-button" style="background-color:#3D62B3;color:#fff;border:0;">Facebook ilə Daxil Ol</a>
        <a href="${createLink(action:'signup', controller:'user')}" class="blue-button" >Qeydiyyatdan keç</a> 

      </g:else>

    </div>


    <div class="clearer"></div>
  </div>
</div> 