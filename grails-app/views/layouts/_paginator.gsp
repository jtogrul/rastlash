<g:if test="${!firstPage}">
    <a href="${createLink(action:action, id:page-1)}">← Daha yenilər</a>
  </g:if>
  
  <g:if test="${!lastPage}">
     ○ 
    <a href="${createLink(action:action, id:page+1)}">Daha köhnələr →</a>
  </g:if>