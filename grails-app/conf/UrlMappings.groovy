class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
                "/r/$rastlanti"(controller: "rastla", action:"rastla")
                "/vote/$vote/$id"(controller: "vote", action:"vote")
                "/home"(controller:"profile", action:"home")
		"/"(controller:"index")
		"500"(view:'/error')
                "404"(controller:"index", action:"notFound")
	}
}
