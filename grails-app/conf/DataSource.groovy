dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
    username = "root"
    password = "0n1y706ru1.n"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            //url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
            url = "jdbc:mysql://localhost/rastlash?useUnicode=yes&characterEncoding=UTF-8"
            username = "root"
            password = "1"
        }
    }
    test {
        dataSource {
            //url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
            url = "jdbc:mysql://localhost/rastlash?useUnicode=yes&characterEncoding=UTF-8"
            username = "root"
            password = "1"
        }
    }
    production {
        dataSource {
            url = "jdbc:mysql://127.0.0.1:3306/rastlash?useUnicode=yes&characterEncoding=UTF-8"
            username = "root"
            password = "0n1y706ru1.n"
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }
    }
}
