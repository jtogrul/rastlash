package rastlash

import grails.converters.JSON
class ProfileController {
    
    def user
    
    def beforeInterceptor = [action:this.&auth]

    def auth() {
        user = User.findWhere(id:session.user)
        if(!user) {
            flash.error = "Sayta yenidən daxil olun"
            redirect(controller:"index")
            return false
        }
    }
    
    def home() {      
        //def user = User.findWhere(id:session.user)
        
        def hasSeen = UserSee.findWhere(user:user)
        def userInterests = user.interests.findAll()
        
        if(session.rastplusurl) {
            redirect(controller:"rastplus", action:"check")
            return
        }
        
        if(!userInterests) {
            redirect(controller:"profile", action:"interests")
            return
        }
        
        //if (user.facebookUid && user.shareFacebookOnLike && !user.facebookToken) {
        //    redirect(controller:"facebook", action:"authent")
        //}
        
        def strSeenWhere = hasSeen? "( p.id not in (select us.post.id from UserSee as us where us.user.id="+user.id+") ) and " :""
        
        def today = new Date()
        
        def rastlaPosts = UserVote.executeQuery("select uv.post as post  from UserVote as uv WHERE uv.post.interest.id in (:userInterests) AND uv.post.enabled = 1 AND uv.vote = true AND uv.time > :yesterday GROUP BY uv.post ORDER BY count(*)",[userInterests:userInterests*.id, yesterday:today.previous()],[max: 10, offset: 0]).asList()
        
        render(view:"/user/home",model:[user:user,posts:rastlaPosts])
    }

    def index() {
        redirect(action:"home")
    }
    
    def posts() {
        //def posts = Post.findAll("from Post as p WHERE p.poster.id = ? ORDER BY p.id DESC", [user.id])
        
        def max = 10
        def page = params.id? Integer.valueOf(params.id) : 1
        
        def c = Post.createCriteria()
        def results = c.list (max: max, offset: max*(page-1)) {
            eq("poster", user)
            order("id", "desc")
        }
        
        def postCount = results.getTotalCount()
        def pagesCount = (postCount).intdiv(max)
        
        if(pagesCount==0) pagesCount = 1
        
        if (postCount % max != 0) pagesCount++
        
        def firstPage = page == 1
        def lastPage = page == pagesCount
        
        if (page < 1 || page > pagesCount ) {
            render (view:"/notFound")
            return
        }
        
        render(view:"/user/posts",model:[posts:results, page:page, firstPage:firstPage, lastPage:lastPage])
    }
    
    def likes() {
        //def likes = UserVote.findAll("from UserVote as uv WHERE uv.user = :user AND uv.vote = true ORDER BY uv.time DESC", [user:user])*.post
        
        def max = 10
        def page = params.id? Integer.valueOf(params.id) : 1
        
        def c = UserVote.createCriteria()
        def results = c.list (max: max, offset: max*(page-1)) {
            eq("user", user)
            eq("vote", true)
            order("time", "desc")
        }
        
        def postCount = results.getTotalCount()
        def pagesCount = (postCount).intdiv(max)
        
        if (postCount % max != 0) pagesCount++
        if(pagesCount==0) pagesCount = 1
        
        def firstPage = page == 1
        def lastPage = page == pagesCount
        
        if (page < 1 || page > pagesCount ) {
            render (view:"/notFound")
            return
        }
        
        render(view:"/user/likes",model:[likes:results*.post, page:page, firstPage:firstPage, lastPage:lastPage])
    }
    
    def history() {
        //def likes = UserVote.findAll("from UserVote as uv WHERE uv.user = :user AND uv.vote = true ORDER BY uv.time DESC", [user:user])*.post
        
        def max = 20
        def page = params.id? Integer.valueOf(params.id) : 1
        
        def c = UserSee.createCriteria()
        def results = c.list (max: max, offset: max*(page-1)) {
            eq("user", user)
            order("id", "desc")
        }
        
        def postCount = results.getTotalCount()
        def pagesCount = (postCount).intdiv(max)
        
        if (postCount % max != 0) pagesCount++
        if(pagesCount==0) pagesCount = 1
        
        def firstPage = page == 1
        def lastPage = page == pagesCount
        
        if (page < 0 || page > pagesCount ) {
            render (view:"/notFound")
            return
        }
        
        render(view:"/user/history",model:[likes:results*.post, page:page, firstPage:firstPage, lastPage:lastPage, pagesCount:pagesCount])
    }
    
    def interests() {
        def userInterests = user.interests.asList()
        def allInterests = Interest.findAll()
        //render userInterests as JSON
        render(view:'/user/interests',model:[userInterests:userInterests,allInterests:allInterests])
    }
    
    def saveinterests() {
        //params.each() { render " ${it}" };
        //render "eylence="+params["eylence"]+" texnologiya="+params["texnologiya"]
        //def u = User.findWhere(id:session.user)
        def userInterests = user.interests.asList()
        def allInterests = Interest.findAll()
        allInterests.each() {
            if(params[it.shortName]=="on") {
                if(! (it in userInterests)) { user.addToInterests(it).save()}
            } else {
                if(it in userInterests) { user.removeFromInterests(it).save() }
            }
        };
        user.save(failOnError: true, flush:true)
        //render u as JSON
        redirect(action:'interests')
        
    }
    
    def socialaccounts() {
        //def u = User.findWhere(id:session.user)
        render(view:'/user/socialaccounts',model:[user:user])
    }
    
    def savefacebooksharing() {
        //def user = User.findWhere(id:session.user)

        user.shareFacebookOnLike = params["sharefacebookonlike"]=="on"
        
        user.save(failOnError: true, flush:true)
        redirect(action:'socialaccounts')
    }
    
}
