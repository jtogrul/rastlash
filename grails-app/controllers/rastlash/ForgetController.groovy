package rastlash

class ForgetController {

    def index() { 
        render(view:"/forget/forget")
    }
    
    def forget() {
        
        def u = User.findWhere(username:params.usernameemail)
        
        if (!u) {
            u = User.findWhere(email:params.usernameemail)
        }
        
        if(u) {
            // TODO improve secret code generation
            def secret = u.username[1..3] + (new Date().time/2)

            new UserForget(user:u, secret:secret).save(failOnError: true)
            
            // TODO send email
            sendMail {
                to u.email
                subject "Rastlash.com - Şifrə bərpası"
                html "Salam, "+u.fullName+",<br/>sizin Rastlash.com hesabınızın şifrəsinin dəyişdirilməsi üçün sorğu göndərilib. Həqiqətən şifrəni dəyişmək istəyirsinizsə bu linkə klikləyin : <a href='http://rastlash.com/forget/recover/"+secret+"'>http://rastlash.com/forget/recover/"+secret+"</a><br/>Əgər siz sorğu göndərməmisizsə nəsə etmənizə ehtiyac yoxdur"
            }
            
            //render "Şifrəni bərpa etmək üçün təlimatlar emailinizə göndərildi. Zəhmət olmasa, "+u.email+" poçtunuzun Gələnlər qutusunu yoxlayın"+secret
            render(view:"/layouts/message", model:[message:"Şifrəni bərpa etmək üçün təlimatlar emailinizə göndərildi. Zəhmət olmasa, "+u.email+" poçtunuzun Gələnlər qutusunu yoxlayın", divclass:"green-box"])
            

        } else {
            flash.error = "Bu istifadəçi adı və ya email ilə qeydiyyat yoxdur. Əsas səhifəyə keçib qeydiyyatdan keçə bilərsiniz"
            redirect(action:"index")
        }
        
        
    }
    
    def recover() {
        def userforget = UserForget.findWhere(secret:params.id)
        
        if (userforget){
            redirect(action:"newpassword", id:params.id, params:[secret:params.id])
            
        } else {
            //render "Səhv: bu istifadəçi şifrə dəyişmək üçün sorğu göndərməyib və ya artıq şifrə dəyişdirilib"
            render(view:"/layouts/message", model:[message:"Səhv: bu istifadəçi şifrə dəyişmək üçün sorğu göndərməyib və ya artıq şifrə dəyişdirilib", divclass:"red-box"])
        }
        
    }
    
    def newpassword() {
        if (request.method == 'POST') {
            def userforget = UserForget.findWhere(secret:params.secret)
        
            if (userforget) {
            
                def u = userforget.user
            
                if((params.password != params.password2) || (params.password.size() > 30) || (params.password.size()<5) ) {
                
                    flash.error = "Şifrə 5 və 30 arası sayda simvoldan ibarət olmalıdır və iki şifrə də eyni olmalıdır"
                    return [user:u]
                } else {
                    u.hashedPassword = params.password.encodeAsPassword()
                    userforget = UserForget.findAllWhere(user:u)
                    userforget*.delete()
                    //render "password updated"
                    render(view:"/layouts/message", model:[message:"Şifrəniz dəyişdirildi", divclass:"green-box"])
                }
            
            } else {
                //render "Səhv: Bu şifrə artıq dəyişdirilib"
                render(view:"/layouts/message", model:[message:"Səhv: Bu şifrə artıq dəyişdirilib", divclass:"red-box"])
            }
        }
    }
    
}
