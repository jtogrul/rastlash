package rastlash

class ManagePostController {
    
    def user
    
    def beforeInterceptor = [action:this.&auth]

    def auth() {
        user = User.findWhere(id:session.user)
        if(!(user.role in ["editor", "admin"])) {
            render "Səhifələrə düzəliş etməyə səlahiyyətiniz yoxdur"
            return false
        }
    } 
    

    static scaffold = Post
    
    def list() {
        [postInstanceList: Post.list(sort:"id", order:"desc"),
            postInstanceTotal: Post.count()]
    }
}
