package rastlash

import grails.converters.*
class FacebookController {

    def appId = "326782320752165"
    def appSecret = "b81d19a8ced0a28b60fe1492a857e731"

    def index = {}

    def authent = {
        def redirectURI = createLink(absolute:true, action: 'callback')
        redirect(url: "https://www.facebook.com/dialog/oauth?client_id=${appId}&redirect_uri=${redirectURI}&scope=publish_stream,offline_access,email")
    }

    def callback = {
        // Request an access token by fetching url with given code.
        def redirectURI = createLink(absolute:true, action: 'callback')
        def rootUrl = "https://graph.facebook.com/oauth/access_token?client_id=${appId}&client_secret=${appSecret}"
        String accessTokenURL = rootUrl + "&redirect_uri=${redirectURI}&code=${params.code.encodeAsURL()}"
        String result = new URL(accessTokenURL).getText()
        // Access token is first key=value pairs value.
        String accessToken = result.tokenize("&")[0].tokenize("=")[1]
        // Use a facebook client to request current logged in user friends.
        String data = new URL("https://graph.facebook.com/me?access_token="+accessToken).getText()
      
        def userJson = JSON.parse(data)
        
        def u = User.findByFacebookUid(userJson.id)
        
        if (u) {
                        
            if(session.user){ // qeydiyyatli istifadeci sosial profil elave edir ve ya sistem token tezeleyir
                
                
                def user = User.findWhere(id:session.user)
                u.facebookToken = accessToken
                u.save()
                
                if (user.facebookUid) { // token tezelenir
                    redirect(controller:'profile' ,action:"home")
                } else { // sosial profil elave edilir
                    render(view:"/layouts/message", model:[message:"Bu facebook hesabı daha əvvəl başqa bir Rastlaş hesabı tərəfindən əlavə olunub. Əgər o sizsinizsə həmin hesabdan daxil olub, bu facebook profilinizlə əlaqəni kəsə bilərsiniz. Yalnız bundan sonra bu facebook profilini indi istifadə etdiyiniz Rastlaş heşabı ilə əlaqələndirə bilərsiniz", divclass:"red-box"])
                }
                return
                
            } else { // qeydiyyatli istifadeci login edir
                session.user = u.id
                response.setCookie("username", u.username, 604800)
                response.setCookie("password", u.hashedPassword, 604800)
                //
                u.facebookToken = accessToken
                u.save()
                redirect(controller:'profile' ,action:"home")
                return
            }
            
        } else {
            
            
            if(session.user) { // qeydiyyatli istifadeci sosial profil elave edir
                
                def user = User.findWhere(id:session.user)
                user.facebookUid = userJson.id
                //
                user.facebookToken = accessToken
                user.save(failOnError: true)
                redirect(controller:"profile", action:"socialaccounts")
                
            } else { //qeydiyyatsiz istifadeci qeydiyyatdan kecir
                session.facebookUid = userJson.id
                //
                session.facebookToken = accessToken
                def user = new User(username:userJson.username, fullName:userJson.name, email:userJson.email)
                render(view:"/user/signup", model:[user:user])
                return
            }
   
        }
      
        render userJson
    }
   
    def remove() {
        def u = User.findWhere(id:session.user)
        u.facebookUid = null
        u.facebookToken = null
        u.save()
        redirect(action:'socialaccounts', controller:'profile')
    }
}
