package rastlash

import grails.converters.XML
import grails.converters.JSON
import org.hibernate.Query
import java.util.regex.Pattern
import java.util.regex.Matcher
import com.sun.jndi.toolkit.url.Uri
class RastlaController {
    
    def cookieService

    def index() {
        
        //
        //render(view:"/layouts/message", model:[message:"Bağışlayın. Sayt tam fəaliyyətə hələ başlamadığı üçün bu funksinallıq işlək deyil. Saytın ilkin versiyası bazanın zənginləşdirilməsi üçün nəzərdə tutulub. Tam fəaliyyətə başlayana qədər yalnız bazaya səhifələr əlavə edə bilərsiniz və bu müddət ərzində əlavə edilmiş səhifələri gəzmək mümkün deyil. Tam fəaliyyətə nə qədər tez başlamağımız bazanın zənginliyindən asılı olacaq. Əgər varsa, öz sayt və ya bloqunuzun səhifələrini ya da sadəcə olaraq hər hansı bir maraqlı səhifəni, şəkli, videonu əlavə edərək bizə dəstək ola bilərsiniz", divclass:"red-box"])
        //return
        //
        
        def user = User.findWhere(id:session.user)
        // if !user >> check user with cookie >> if user -> set session
        if (!user) {
            user = User.findWhere(username: request.getCookie("username"), hashedPassword: request.getCookie("password"))
            if(user) {
                session.user = user.id
            }
        }
        
        if(!user) {
            flash.error = "Səhifələrlə rastlaşmaq üçün hesabınıza daxil olun"
            redirect(controller:"index")
            return
        }
        
        def hasSeen = UserSee.findWhere(user:user)
        def userInterests = user.interests.findAll()
        
        if(!userInterests) {
            redirect(controller:"profile", action:"interests")
            return
        }
        
        def strSeenWhere = hasSeen? "( p.id not in (select us.post.id from UserSee as us where us.user.id="+user.id+") ) and " :""
        
        def rastlaPost = Post.executeQuery("select p.id from Post as p  where ( "+strSeenWhere+"(p.interest.id in (:userInterests)) AND (p.enabled = 1) ) order by p.reviewed DESC, p.seen DESC",[userInterests:userInterests*.id],[max: 1, offset: 0])
        
        if(rastlaPost) {
            redirect(url:"/r/"+rastlaPost.first())
        } else {
            render(view:"/layouts/message", model:[message:"Bağışlayın. Maraq dairələrinizdən sizə göstərəcək başqa səhifə qalmadı. Rastlaş-dakı səhifələr istifadəçilər tərəfindən əlavə olunur. Siz də səhifələrin artırılmasına dəstək ola bilərsiniz, sadəcə profilinizə qayıdın və 'Səhifə əlavə et' seçin", divclass:"red-box"])
        }
        
        
        
    }
    
    def rastla() {
        
        //
        //render(view:"/layouts/message", model:[message:"Bağışlayın. Sayt tam fəaliyyətə hələ başlamadığı üçün bu funksinallıq işlək deyil. Saytın ilkin versiyası bazanın zənginləşdirilməsi üçün nəzərdə tutulub. Tam fəaliyyətə başlayana qədər yalnız bazaya səhifələr əlavə edə bilərsiniz və bu müddət ərzində əlavə edilmiş səhifələri gəzmək mümkün deyil. Tam fəaliyyətə nə qədər tez başlamağımız bazanın zənginliyindən asılı olacaq. Əgər varsa, öz sayt və ya bloqunuzun səhifələrini ya da sadəcə olaraq hər hansı bir maraqlı səhifəni, şəkli, videonu əlavə edərək bizə dəstək ola bilərsiniz", divclass:"red-box"])
        //return
        //
        
        
        
        def post =  Post.findById(params.rastlanti)
        
        
        
        if(!post) {
            response.sendError(404)
            return
        }
        
        if(!post.enabled) {
            render(view:"/layouts/message", model:[message:"Bu səhifə içərisindəki məzmuna görə moderator tərəfindən qadağan olunub.", divclass:"red-box"])
            return
        }
        
        
        
        def votelike = false
        def votedislike = false
        def loggedin = false
        def isManager = false
        
        if(session.user) {
            def user = User.findWhere(id:session.user)
            
            if (!user) {
                user = User.findWhere(username: request.getCookie("username"), hashedPassword: request.getCookie("password"))
                if(user) {
                    session.user = user.id
                }
            }
            
            if(user) {
                loggedin = true
                isManager = user.role in ["editor", "admin"]
                
                if(! UserSee.findWhere(user:user,post:post)) {
                    new UserSee(user:user,post:post).save(failOnError: true)
                }
                
                def uservote = UserVote.findWhere(user:user,post:post)
                if(uservote) {
                    if (uservote.vote) {
                        votelike = true
                    } else {
                        votedislike = true
                    }
                }
            }
        }

        post.seen = post.seen+1
        post.save()
        
        def posturl = URLEncoder.encode("http://rastlash.com/r/"+post.id)
        
        def iframeurl = post.url
        if(iframeurl.contains("youtube.com/")) {
            def m  = iframeurl =~ /http.*\\?v=([a-zA-Z0-9_\\-]+)(?:&.)*/
            iframeurl = "http://www.youtube.com/embed/"+m[0][1]+"?rel=0"
        }

        render(view:"/rastla/rastla",model:[post:post,id:params.rastlanti,votelike:votelike,votedislike:votedislike,loggedin:loggedin,posturl:posturl,iframeurl:iframeurl,isManager:isManager])
    }
    
    def report() {
        
        
        if (request.method == 'POST') {
            
            sendMail {
                to "ceferlitogrul@gmail.com"
                subject "Rastlaş - Şikayət"
                html "Post: <a href='http://rastlash.com/r/"+params.postid+"'>id:"+params.postid+"</a>,<br/>Səbəb: "+params.sebeb+"<br/>Qeyd: "+params.qeyd
            }
            
            render(view:"/layouts/message", model:[message:"Şikayətiniz qəbul olundu. Təşəkkürlər", divclass:"green-box"])
            return
            
        } else {
            def post = Post.findWhere(id:params.id.toLong())
            
            if(!post) {
                render(view:"/layouts/message", model:[message:"Belə səhifə tapılmadı", divclass:"red-box"])
                return
            } else {
                render(view:"/rastla/report", model:[postid:params.id])
                return
            }
        }
        
        
    }
}
