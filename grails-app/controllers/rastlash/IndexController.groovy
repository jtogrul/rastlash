package rastlash

class IndexController {
    
    def cookieService

    def index() {
        if(session.user) {
            redirect(controller: "home")
        } else {
            //check cookie here
            // if correct cookie >> set session
            def u = User.findWhere(username: request.getCookie("username"), hashedPassword: request.getCookie("password"))
            if(u) {
                session.user = u.id
                response.setCookie("username", u.username, 604800)
                response.setCookie("password", u.hashedPassword, 604800)
                redirect(controller: "home")
                return
            }
            
            
            render(view:"/index")
        }
        
    }
    
    def notFound() {
        render(view:'/notFound')
    }
}
