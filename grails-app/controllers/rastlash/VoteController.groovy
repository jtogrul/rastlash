package rastlash

import grails.converters.JSON
class VoteController {

    def index() { 
    }
    
    def vote() {
        
        def response
        
        def post = Post.findWhere(id:params.id.toLong())
        if(post) {
        
            if(session.user) {
                def user = User.findWhere(id:session.user)
                if(user){
                    def uservote = UserVote.findWhere(user:user,post:post)
                    if (uservote) {
                        if(uservote.vote != (params.vote == "like")) {
                            uservote.vote = !uservote.vote
                            uservote.save()
                            
                            response = [status:"ok",vote:params.vote]

                        } else {
                            uservote.delete()
                            response = [status:"ok",vote:"none"]
                        }
                        
                        
                            
                    } else {
                        new UserVote(user:user,post:post,vote: params.vote=="like").save()
                        response = [status:"ok",vote:params.vote]
                    }
                    
                    //facebook share
                    if(params.vote == "like" && user.facebookUid && user.shareFacebookOnLike) { 
                        def posturl = "http://rastlash.com/r/"   
                        def _url = "https://graph.facebook.com/" + user.facebookUid + "/rastlash:like?"
                        _url += "&access_token=326782320752165|jJ_XpCzvivIZPOc7mbl5R2ks5YI"
                        _url += "&page="+posturl+post.id
                        _url += "&method=post"

                        try {
                            String data = new URL(_url).getText()
                        } catch (Throwable t) {
                        } finally {
                        }
                    }
                    //
                    
                    /* old fb share
                    if(params.vote == "like" && user.facebookToken && user.shareFacebookOnLike) {
                                
                    def posturl = "http://rastlash.com/r/"
                                
                    def _url = "https://graph.facebook.com/" + user.facebookUid + "/feed?"
                    //_url += "message=MSG_STRING"
                    _url += "&access_token=" + user.facebookToken
                    _url += "&name="+URLEncoder.encode(post.title, "UTF-8")
                    _url += "&link="+URLEncoder.encode(posturl+post.id, "UTF-8")
                    //_url += "&description=DESCRIPTION_STRING";
                    _url += "&method=post"

                    String data = new URL(_url).getText()
                    }
                     */  

                    
                } else {
                    response = [status:"error",message:"Bağışlayın. Belə istifadəçi mövcud deyil."]
                }
            } else {
                response = [status:"error",message:"Xahiş edirik, sayta yenidən giriş edin. Əgər sayta üzv olmamışsınızsa Qeydiyyatdan keçin"]
            }
        } else {
            response = [status:"error",message:"Bağışlayın. Səs verməyə çalışdığınız səhifə mövcud deyil"]
        }
        
        render response as JSON
    }
    
}
