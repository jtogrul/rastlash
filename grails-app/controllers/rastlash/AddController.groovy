package rastlash

import grails.converters.JSON
class AddController {
    
    def user
    
    def beforeInterceptor = [action:this.&auth]

    def auth() {
        user = User.findWhere(id:session.user)
        if(!user) {
            flash.error = "Sayta yenidən daxil olun"
            redirect(controller:"index")
            return false
        }
    }
    
    def index() {
        redirect(action:'webpage')
    }
    
    def webpage() {

        def allInterests = Interest.findAll()
        if (request.method == 'POST') {
            
            def post = new Post(params)
            post.poster = user
            if (! post.validate()) {
                return [post:post, allInterests:allInterests]
            } else {
                post.save()
                user.addToPosts(post).save(failOnError:true)
                
                def uv = new UserVote(user:user, post:post)
                uv.vote = true
                uv.save(failOnError: true)
                
                redirect(controller:'profile', action:'posts')
                return
            }
        } else {
            def post = new Post(url:session.rastplusurl, title:session.rastplustitle)
            //invalidate rastplus-ing if there was any
            session.rastplusurl = null
            session.rastplustitle = null
            
            return [allInterests:allInterests, post:post]
        }
    }
    
    
}
