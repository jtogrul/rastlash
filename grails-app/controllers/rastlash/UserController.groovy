package rastlash

import grails.converters.JSON
class UserController {

    def index() { }
    
    def login() {
        
        User u = User.findWhere(username: params.username, hashedPassword: params.password.encodeAsPassword())
        
        if(!u) {
            u = User.findWhere(email: params.email, hashedPassword: params.password.encodeAsPassword())
        }
        
        if (u) {
            session["user"] = u.id
            
            //set cookie here
            response.setCookie("username", u.username, 604800)
            response.setCookie("password", u.hashedPassword, 604800)
            
            /* removed because no need to token after timeline integration
            if(u.facebookUid) {
                redirect(controller:'facebook' ,action:"authent")
            } else {
                redirect(controller:'profile' ,action:"home")
            }*/
            
            redirect(controller:'profile' ,action:"home")
            
            return
            
        } else {
            flash.error = "Giriş detalları düzgün deyil"
            redirect(controller:"index")
            return
        }
        
    }
    
    def logout() {
        session.invalidate()
        // delete cookie
        response.deleteCookie("username")
        response.deleteCookie("password")
        
        redirect(controller: "index")
        return
    }
    
    def signup() {
        
        if (request.method == 'POST') {
            def u = new User(params)
            
            if (session.facebookUid) {
                u.facebookUid = session.facebookUid
            }
            
            if (session.facebookToken) {
                u.facebookToken = session.facebookToken
            }
            
            if (! u.save()) {
                return [user:u]
            } else {
                session.user = u.id
                response.setCookie("username", u.username, 604800)
                response.setCookie("password", u.hashedPassword, 604800)
                redirect(controller:'profile', action:'home')
                return
            }
        } else if (session.user) {
            redirect(controller:'profile' ,action:"home")
            return
        }
        
    }
}
