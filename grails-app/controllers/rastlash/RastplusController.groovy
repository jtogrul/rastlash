package rastlash

class RastplusController {
    

    def index() {
        
        if (params.url) {
            def url = URLDecoder.decode(params.url, "UTF-8")
            
            URL xurl = new URL(url);
            URI uri = new URI(xurl.getProtocol(), xurl.getUserInfo(), xurl.getHost(), xurl.getPort(), xurl.getPath(), xurl.getQuery(), xurl.getRef());
            def encodedurl = uri.toASCIIString()
            
            def pattern = ~/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/ 
            
            if(pattern.matcher(encodedurl).matches()) {
                session.rastplusurl = encodedurl
            } else {
                render(view:"/layouts/message", model:[message:"Səhv: Rast+lanılacaq səhifənin ünvanı düzgün yazılmayıb.", divclass:"red-box"])
                return
            }
            
            
        } else {
            render(view:"/layouts/message", model:[message:"Səhv: Rast+lanılacaq səhifənin ünvanı düzgün yazılmayıb.", divclass:"red-box"])
            return
        }
        
        
        if (params.title) session.rastplustitle = URLDecoder.decode(params.title)
        
        if(session.user) {
            redirect(controller:"rastplus", action:"check")
            return
        } else {
            flash.error = 'Giriş etdikdən sonra bu ünvanı "Rast+"laya bilərsiniz'
            redirect(controller:"index")
            return
        }
    }
    
    def check() {
        def user = User.findWhere(id:session.user)
        
        //vote up for rast+ if there is any
        if(session.rastplusurl) {
            def p = Post.findWhere(url:session.rastplusurl)
            
            if (p) {
                
                def uv = UserVote.findWhere(user:user, post:p)
                if (uv) {
                    if(uv.vote == true) {
                        //render "bu səhifəni artıq bəyənmisiniz"
                        render(view:"/layouts/message", model:[message:"Bu səhifəni artıq bəyənmisiniz", divclass:"blue-box"])

                        session.rastplusurl = null
                        session.rastplustitle = null
                        return
                    } else {
                        uv.vote = true
                        uv.save(failOnError: true)
                        //render "rast+landı"
                        render(view:"/layouts/message", model:[message:"Əla! Bu səhifə bəyəndikləriniz sırasına əlavə olundu", divclass:"green-box"])
                        session.rastplusurl = null
                        session.rastplustitle = null
                        return
                    }
                } else {
                    new UserVote(user:user, post:p, vote:true).save(failOnError: true)
                    //render "Rast+landı"
                    render(view:"/layouts/message", model:[message:"Əla! Bu səhifə bəyəndikləriniz sırasına əlavə olundu", divclass:"green-box"])

                    //invalidate rastplus-ing if there was any
                    session.rastplusurl = null
                    session.rastplustitle = null
                    return 
                }
                
                //add to seen rastplused pages
                if(!UserSee.findWhere(user:user, post:p)) {
                    new UserSee(user:user, post:p).save()
                }
                
                
            } else {
                redirect(controller:"add", action:"webpage")
                return
            }
        } 
    }
    
    def plugin() {
        
        def type = params.type? params.type: "button"

        if(params.url) {

            def url = URLDecoder.decode(params.url, "UTF-8")
            URL xurl = new URL(url);
            URI uri = new URI(xurl.getProtocol(), xurl.getUserInfo(), xurl.getHost(), xurl.getPort(), xurl.getPath(), xurl.getQuery(), xurl.getRef());
            def encodedurl = uri.toASCIIString()
            
            def encodedurl2 = URLEncoder.encode(encodedurl, "UTF-8")
            
            
            def p = Post.findWhere(url:encodedurl)
            def u = User.findWhere(username: request.getCookie("username"), hashedPassword: request.getCookie("password"))
            if(u) {
                session.user = u.id
                response.setCookie("username", u.username, 604800)
                response.setCookie("password", u.hashedPassword, 604800)
            }
            
            def activevote = UserVote.findWhere(post:p, user:u, vote:true)
            
            def active = activevote ? true: false
                
            def likecount = UserVote.countByPostAndVote(p, true)
                               
            if (type == "button") {
                render (view: "/rastplus/button", model:[pageurl:encodedurl2, likecount:likecount, active:active])
            } else {
                render (view: "/rastplus/box", model:[pageurl:encodedurl2, likecount:likecount, active:active])
            }
                
            return
                
        } else {
            render "Səhv!"
            return
        }
    }
    
    
}
