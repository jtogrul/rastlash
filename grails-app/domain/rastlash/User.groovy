
package rastlash

class User {
    
    String fullName
    String username
    String email
    String hashedPassword
    String password
    String password2 
    String role = "user"
    
    
    String facebookUid
    String facebookToken
    boolean shareFacebookOnLike = true
    
    String twitterToken
    String twitterSecret
    boolean shareTwitterOnLike = true
    
    static hasMany = [posts:Post, interests:Interest]
    static mappedBy = [posts:'poster']
    
    static transients = ['password','password2']

    static constraints = {
        username size: 5..15, blank: false, unique: true
        email email: true, blank: false, unique:true
        password  blank:false, bindable:true, size:5..30, matches:/[\S]+/, validator:{ val, obj ->
            if (obj.password2 != null && obj.password != obj.password2)
            return 'user.password.dontmatch'
        }
        password2 nullable: true, bindable: true
        hashedPassword nullable:true
        role(inList:["user", "editor", "admin"], nullable:true, bindable:false)
        facebookUid nullable:true
        facebookToken nullable:true
        shareFacebookOnLike nullable:true
        twitterToken nullable:true
        twitterSecret nullable:true
        shareTwitterOnLike nullable:true
    }
    
    def beforeInsert() {
        if(!role) role = "user"
        hashedPassword = password.encodeAsPassword()
    }
    
    def beforeUpdate() {
        if(password != null) hashedPassword = password.encodeAsPassword()
    }
    
    String toString() {
        return id+ ": "+fullName
    }    
}
