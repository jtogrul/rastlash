package rastlash

class Interest {
    
    String name
    String shortName

    static constraints = {
    }
    
    String toString() {
        return name
    }
}
