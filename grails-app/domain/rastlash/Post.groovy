package rastlash

class Post {
    
    String title
    String type
    String url
    String data
    int seen
    String strNote
    String lang = "az"
    Interest interest
    boolean hasNote = false
    boolean reviewed = false
    boolean enabled = true 
    
    static belongsTo = [poster:User]

    static constraints = {
        title blank:false
        data nullable:true
        type nullable:true
        seen nullable:true
        poster nullable:true
        url blank:false, unique:true, matches: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/ 
        lang inList: ["az", "en", "ru", "tr"], blank:false
        strNote nullable:true
        hasNote nullable:true
        reviewed nullable:true
        enabled nullable:true
    }
    
    def beforeInsert() {
       seen = 0
       type = 1 // webpage
       if (hasNote == false) {
           strNote = ""
       }
   }
   
    static mapping = {
        strNote type: 'text'
    }
   


}
