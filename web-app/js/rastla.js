$(document).ready(function(){
    
    if(hasnote) {
        $("#bar-qeyd").click(function(){
            $("#post-note").dialog();
        }); 
          
        if(lang != 'az') {
            $("#post-note").dialog();
        }
    }
        
        
        
    $(".vote").click(function() {
        
        voteNone();
        vote($(this).attr("vote"));
          
        $.ajax({
            url:homeurl+"vote/"+$(this).attr("vote")+"/"+postid, 
            success:function(result){
                if (result["status"] == "ok") {
                    if(result["vote"] == "none") {
                        voteNone();
                    } else {
                        voteNone();
                        vote(result["vote"]);
                    }
                } else if (result["status"] == "error") {
                    alert(result["message"]);
                }
            }
        }); 
          
    });
        
        
    $("#share-facebook").click(function(){
        popitup("https://www.facebook.com/sharer/sharer.php?u=http://rastlash.com/r/"+postid+"&t="+title);
    });

    $("#share-twitter").click(function(){
        popitup("https://twitter.com/intent/tweet?text="+encodeURIComponent(title)+"&via=rastlash&hashtags=rastlash&url=http://rastlash.com/r/"+postid);
    });

    $("#share-googleplus").click(function(){
        popitup("https://plus.google.com/share?url=http://rastlash.com/r/"+postid);
    });
        
})
      
function voteNone(){
    $(".vote").removeClass("active-vote");
}
      
function vote(votename) {
    $("#"+votename+"-vote").addClass("active-vote");
}
      
function popitup(url) {
    newwindow=window.open(url,'name','height=350,width=640');
    if (window.focus) {
        newwindow.focus()
        }
    return false;
}